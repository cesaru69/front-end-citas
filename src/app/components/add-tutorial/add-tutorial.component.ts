import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/services/tutorial.service';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-tutorial.component.html',
  styleUrls: ['./add-tutorial.component.css']
})
export class AddTutorialComponent implements OnInit {
  cita = {
    idConsultorio: 1,
    idDoctor: 1,
    nombrePaciente : "Esteban Dominguez Camaños"
  };
  submitted = false;

  constructor(private tutorialService: TutorialService) { }

  ngOnInit(): void {
  }

  saveCita(): void {
    const data = {
      idConsultorio: this.cita.idConsultorio,
      idDoctor : this.cita.idDoctor,
      nombrePaciente: this.cita.nombrePaciente
    };

    this.tutorialService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newTutorial(): void {
    this.submitted = false;
    this.cita = {
      idConsultorio: 1,
      idDoctor: 1,
      nombrePaciente : "Esteban Dominguez Camaños"
    };
  }

}
